package uz.azn.lesson25

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.BatteryManager
import android.util.Log
import kotlin.math.log

class BatteryBroadcast:BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {

        val level = intent?.getIntExtra(BatteryManager.EXTRA_LEVEL, 0)

        Log.d("battery", "$level")
    }
}