package uz.azn.lesson25

import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class ThreeActivity : AppCompatActivity() {

    lateinit var networkBroadcast :NetworkBroadcast
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_three)

         networkBroadcast = NetworkBroadcast()
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(networkBroadcast, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))

    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(networkBroadcast)
    }


}