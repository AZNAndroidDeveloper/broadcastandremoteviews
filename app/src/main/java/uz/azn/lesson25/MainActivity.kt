package uz.azn.lesson25

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.LinkAddress
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.RemoteViews

class MainActivity : AppCompatActivity() {
    val chanel_id = 100
    lateinit var notificationManager:NotificationManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun sendNotification(view: View) {

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(chanel_id.toString(),"Acer",importance)
           notificationChannel.setSound(null,null)

            val intent = Intent(applicationContext,MainActivity::class.java)
//            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://youtebe.com"))
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            val pendingIntent = PendingIntent.getActivity(this,100,intent,0)
// bosganda layoutga kirish uchun yani custom yasash uchun
            val remoteViews = RemoteViews(packageName,R.layout.notification_layout)
            remoteViews.setTextViewText(R.id.tv_title,"Android Uz")
//            remoteViews.setTextViewText(R.id.tv_desc,"Salom AndroidChilar")
            remoteViews.setOnClickPendingIntent(R.id.custom_layout, pendingIntent)

// bir nechta habar kelishi aniqlab beradi
            val messagingStyle = Notification.MessagingStyle("Shaxzod")
            messagingStyle.conversationTitle = "Uzbek developer"
            messagingStyle.addMessage("Barchag salom",System.currentTimeMillis(),"Zokirjon")
            messagingStyle.addMessage("Salom Sizga ham",System.currentTimeMillis(),"Asilbek")
            messagingStyle.addMessage("Bayramlariz bilan",System.currentTimeMillis(),"Jamolldin")

            val noticationBuilder = Notification.Builder(this,chanel_id.toString())
                    .setSmallIcon(R.drawable.ic_house)
                    .setContentTitle("Xabar keldi")
                    .setContentText("Siza yangi habar keldi")
//                    .setStyle(messagingStyle)
                .setContent(remoteViews)

//                    .setStyle(Notification.InboxStyle()
//                            .addLine("Birinchi habar")
//                            .addLine("Ikkinchi  habar")
//                            .addLine("Uchinchi habar")
//                    )
//                    .addAction(R.drawable.ic_house,"Dasturni ochish",pendingIntent)

            notificationManager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
            notificationManager.notify(0,noticationBuilder.build())


        }

    }
}