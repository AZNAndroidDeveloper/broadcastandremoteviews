package uz.azn.lesson25

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.util.Log

class NetworkBroadcast:BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {

        if (isOnline(context)){
            Log.d("TAG", "internet bor")
        }
        else{
            Log.d("TAG", "internet yoq")

        }
    }

    fun isOnline(context: Context?):Boolean{
        val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo

        return activeNetworkInfo!=null && activeNetworkInfo.isConnected
    }
}