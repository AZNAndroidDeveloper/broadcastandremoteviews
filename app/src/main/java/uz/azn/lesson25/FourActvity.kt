package uz.azn.lesson25

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class FourActvity : AppCompatActivity() {
    lateinit var broadcast: BatteryBroadcast
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_four_actvity)
        broadcast =BatteryBroadcast()
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(broadcast, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcast)
    }


}