package uz.azn.lesson25

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RemoteViews

class SecondAcitvity : AppCompatActivity() {
    val chanel_id = 100
    lateinit var notificationManager: NotificationManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second_acitvity)

    /// bu activityda broadReciver yasaymiz va  remot input
    }
    fun sendNotification(view: View) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(chanel_id.toString(), "Acer", importance)
            notificationChannel.setSound(null, null)

            val intent = Intent(applicationContext, NotificationBroadcast::class.java)
            intent.action = "my_notification"
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            val pendingIntent = PendingIntent.getBroadcast(this, 100, intent, 0)

            val remoteViews = RemoteViews(packageName, R.layout.notification_layout)
            remoteViews.setTextViewText(R.id.tv_title, "Android Uz")
            remoteViews.setOnClickPendingIntent(R.id.custom_layout, pendingIntent)

            val remoteInput  = RemoteInput.Builder("key").setLabel("Yozing").build()
            val notificationAction = Notification.Action.Builder(R.drawable.ic_house,"javob berish",pendingIntent)
                .addRemoteInput(remoteInput).build()


            val noticationBuilder = Notification.Builder(this, chanel_id.toString())
                .setSmallIcon(R.drawable.ic_house)
                .setContentTitle("Xabar keldi")
                .setContentText("Siza yangi habar keldi")
                .addAction(notificationAction)



            notificationManager =
                this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
            notificationManager.notify(0, noticationBuilder.build())


        }
    }
}