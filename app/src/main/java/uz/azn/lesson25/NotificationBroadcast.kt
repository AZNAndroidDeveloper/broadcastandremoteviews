package uz.azn.lesson25

import android.app.RemoteInput
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class NotificationBroadcast:BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action =="my_notification"){
            val action = RemoteInput.getResultsFromIntent(intent)
            val text = action.getString("key")
            Log.d("user", "$text")
        }
    }
}